import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-appareil', // <-- balise niv2
  templateUrl: './appareil.component.html',
  styleUrls: ['./appareil.component.scss']
})
export class AppareilComponent implements OnInit {

  @Input() appareilName: string; // utilisation du décorateur
  @Input() appareilStatus: string;     

  getColor() {
    if(this.appareilStatus === 'on') {
      return 'green';
    }else if(this.appareilStatus === 'off') {
      return 'red';
    }
  }

  constructor() { }

  testSI: string = 'String interpolation simple'; //  binding de base

  ngOnInit(): void {
  }

  getStatus() {
    return this.appareilStatus; 
  }

}
