import { Component } from '@angular/core';

@Component({
  selector: 'app-root',  // <-- balise niv1 
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  isAuth = false;

  title = 'Mes appareils';  

  lastUpdate = new Date();

  appareils = [
    { // propriété name et stats
      name: 'Machine à laver',
      status: 'off'
    },
    {
      name: 'Frigo',
      status: 'on'
    },
    {
      name: 'Ordinateur',
      status: 'off'
    }
  ];

  
  constructor() {
    setTimeout( // property binding
      () => {
        this.isAuth = true;
      }, 4000
    );
  }

  onAllumer() {  // event binding
    console.log('On allume tout !');
  }


}
